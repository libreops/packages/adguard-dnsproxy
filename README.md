# Adguard dnsproxy

a Simple DNS proxy with DoH, DoT, DoQ and DNSCrypt support.

[Source code](https://github.com/AdguardTeam/dnsproxy)

## Download

Download dnsproxy package for:

- [Ubuntu/Debian package amd64](https://gitlab.com/libreops/packages/adguard-dnsproxy/-/jobs/artifacts/main/browse?job=run-build)

## Install

```bash
sudo apt -y install adguard-dnsproxy*amd64.deb

```
