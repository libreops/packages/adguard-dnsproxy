#!/bin/bash

fpm -n adguard-dnsproxy \
    -v ${VERSION}       \
    -a x86_64           \
    -s dir              \
    -t deb              \
    --description "Simple DNS proxy with DoH, DoT, DoQ and DNSCrypt support" \
    --license "Apache 2.0"                              \
    --maintainer "Evaggelos Balaskas"                   \
    --url "https://github.com/AdguardTeam/dnsproxy"     \
    --vendor "AdGuard"                                  \
    --conflicts dnsproxy				\
    --config-files /etc/conf.d/dnsproxy                 \
    --deb-no-default-config-files                       \
    --deb-systemd dnsproxy.service                      \
    --after-install after-install.sh			\
    dnsproxy.config=etc/conf.d/dnsproxy                 \
    dnsproxy.socket=/usr/lib/systemd/system/            \
    dnsproxy=/usr/bin/dnsproxy

